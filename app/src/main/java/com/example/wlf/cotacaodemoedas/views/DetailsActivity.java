package com.example.wlf.cotacaodemoedas.views;

import android.app.Activity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.wlf.cotacaodemoedas.R;
import com.example.wlf.cotacaodemoedas.classes.Get;

public class DetailsActivity extends Activity
{
    public static TextView maxDia;
    public static TextView minDia;
    public static TextView lastUpdate;
    public static TextView title;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_details );

        maxDia =        findViewById( R.id.txt_max_dia );
        minDia =        findViewById( R.id.txt_min_dia );
        lastUpdate =    findViewById( R.id.txt_ultima_atualizacao );
        title =         findViewById( R.id.txt_titulo );

        getWindow().addFlags( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON ); //força a tela a ficar ligada

        getWindow().addFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN ); // NÃO exibe a barra superior do android

        Get process = new Get();
        process.execute();
    }
}
