package com.example.wlf.cotacaodemoedas.classes;

import android.os.AsyncTask;
import com.example.wlf.cotacaodemoedas.views.DetailsActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.example.wlf.cotacaodemoedas.views.MainActivity.URL;

public class Get extends AsyncTask< Void, Void, Void >
{
    String data = "";
    String dataParsed = "";
    String dataParsed1 = "";
    String dataParsed2 = "";
    String dataParsed3 = "";
    String singleParsed = "";

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            URL url = new URL(URL);

            HttpURLConnection http = ( HttpURLConnection ) url.openConnection();
            InputStream input = http.getInputStream();
            BufferedReader buffer = new BufferedReader( new InputStreamReader(input) );

            String line = "";

            while ( line != null )
            {
                line = buffer.readLine();
                data += line;
            }

            JSONArray JA = new JSONArray( data );

            for ( int w = 0; w < JA.length(); w++ )
            {
                JSONObject JO = ( JSONObject ) JA.get( w );
                singleParsed = "" + JO.get( "name" ) + "\n" +
                        "R$ " + JO.get( "bid" );
                dataParsed += singleParsed;
            }

            for ( int w = 0; w < JA.length(); w++ )
            {
                JSONObject JO = ( JSONObject ) JA.get( w );
                singleParsed = "Máxima do dia: " + "\n" +
                        "R$ " + JO.get( "high" ) + "\n";
                dataParsed1 += singleParsed;
            }

            for ( int w = 0; w < JA.length(); w++ )
            {
                JSONObject JO = ( JSONObject ) JA.get( w );
                singleParsed = "Mínima do dia: " + "\n" +
                        "R$ " + JO.get("low") + "\n";
                dataParsed2 += singleParsed;
            }

            for ( int w = 0; w < JA.length(); w++ )
            {
                JSONObject JO = ( JSONObject ) JA.get( w );
                singleParsed = "Última atualização: " + "\n" +
                        JO.get( "create_date" ) + "\n";
                dataParsed3 += singleParsed;

            }
        }

        catch ( MalformedURLException e )
        {
            e.printStackTrace();
        } catch ( IOException e )
        {
            e.printStackTrace();
        } catch ( JSONException e )
        {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute( Void aVoid )
    {
        String separador1 = this.dataParsed3;
        String[] divisor1 = separador1.split( " " );

        String[] dataCorreta = divisor1[2].split( "-" );

        DetailsActivity.title.setText( this.dataParsed );
        DetailsActivity.maxDia.setText( this.dataParsed1 );
        DetailsActivity.minDia.setText( this.dataParsed2);
        DetailsActivity.lastUpdate.setText(  divisor1[0] + " "
                +  divisor1[1] + "\n"
                + divisor1[3].trim()
                + "\n" + dataCorreta[2].trim()
                + "/" +  dataCorreta[1]
                + "/"+dataCorreta[0].trim() );
    }
}