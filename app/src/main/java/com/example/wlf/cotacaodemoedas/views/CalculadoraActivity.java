package com.example.wlf.cotacaodemoedas.views;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.wlf.cotacaodemoedas.R;
import com.example.wlf.cotacaodemoedas.classes.Mock;
import com.example.wlf.cotacaodemoedas.classes.GetAccount;
import com.example.wlf.cotacaodemoedas.utils.NetworkUtils;

import java.util.List;

public class CalculadoraActivity extends Activity implements View.OnClickListener{

    public static String URL = "";
    public static EditText valorMoeda;
    public static TextView valorMoeda1;
    private Button btnCalc;
    private Spinner allCurrencies;
    private Spinner bitcoin;
    private ScrollView linearLayoutCalcActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); //força a tela a ficar ligada

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN); // NÃO exibe a barra superior do android

        allCurrencies = findViewById(R.id.spinnerBitcoin);
        bitcoin = findViewById(R.id.spinnerAllCurrencies);
        btnCalc = findViewById(R.id.btn_calcular);
        valorMoeda = findViewById(R.id.edit_valor);
        valorMoeda1 = findViewById(R.id.edit_valor1);
        linearLayoutCalcActivity = findViewById( R.id.linear_layout_calculadora_activity );

        loadSpinnerAllCurrencies();
        loadSpinnerBitcoin();

        btnCalc.setOnClickListener(this);
    }

    private void loadSpinnerAllCurrencies()
    {
        try
        {
            List< String > list = Mock.INSTANCE.getCurrenciesMock();
            ArrayAdapter< String > adapter = new ArrayAdapter<>
                    (
                            this, android.R.layout.simple_spinner_dropdown_item, list
                    );
            this.bitcoin.setAdapter( adapter );

            /*ArrayAdapter<String> adapter = new ArrayAdapter<String>( this, android.R.layout.simple_list_item_1, moedas );
            adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
            allCurrencies.setAdapter( adapter ); */

        }
        catch ( Exception ex )
        {
            showMsg( getString( R.string.error ) + ex.getMessage() );
        }
    }

    private void loadSpinnerBitcoin()
    {
        try {
            List< String > list = Mock.INSTANCE.getBitcoinMock();
            ArrayAdapter< String > adapter = new ArrayAdapter<>
                    (
                            this, android.R.layout.simple_spinner_dropdown_item, list
                    );
            this.allCurrencies.setAdapter( adapter );

        }
        catch ( Exception ex )
        {
            showMsg( getString( R.string.error ) + ex.getMessage() );
        }
    }

    private void showMsg( String msg )
    {
        Snackbar.make( linearLayoutCalcActivity, msg, Snackbar.LENGTH_LONG ).show();
    }

    @Override
    public void onClick( View view )
    {
        int id = view.getId();

        if ( id == R.id.btn_calcular )
        {
            if ( bitcoin.getSelectedItemPosition() == 1 )
            {
                valorMoeda1.setText(URL);
                GetAccount pjc = new GetAccount();
                pjc.converterBitCoin( valorMoeda1, this, "EUR" , valorMoeda.getText().toString() );

            }
            else if( bitcoin.getSelectedItemPosition() == 2 )
            {
                valorMoeda1.setText(URL);
                GetAccount pjc = new GetAccount();
                pjc.converterBitCoin( valorMoeda1, this, "USD", valorMoeda.getText().toString() );
            }
            else if( bitcoin.getSelectedItemPosition() == 3)
            {
                valorMoeda1.setText(URL);
                GetAccount pjc = new GetAccount();
                pjc.converterBitCoin( valorMoeda1, this, "GBP", valorMoeda.getText().toString() );
            }
            else if( bitcoin.getSelectedItemPosition() == 4 )
            {
                valorMoeda1.setText(URL);
                GetAccount pjc = new GetAccount();
                pjc.converterBitCoin(valorMoeda1, this, "RUB", valorMoeda.getText().toString() );
            }
            else if( bitcoin.getSelectedItemPosition() == 5 )
            {
                valorMoeda1.setText(URL);
                GetAccount pjc = new GetAccount();
                pjc.converterBitCoin( valorMoeda1, this, "JPY", valorMoeda.getText().toString() );
            }
            else if( bitcoin.getSelectedItemPosition() == 6)
            {
                valorMoeda1.setText(URL);
                GetAccount pjc = new GetAccount();
                pjc.converterBitCoin( valorMoeda1, this, "CAD", valorMoeda.getText().toString() );
            }
            else
            {
                valorMoeda1.setText(URL);
                GetAccount pjc = new GetAccount();
                pjc.converterBitCoin( valorMoeda1, this, "BRL", valorMoeda.getText().toString() );
            }
        }
    }

}

