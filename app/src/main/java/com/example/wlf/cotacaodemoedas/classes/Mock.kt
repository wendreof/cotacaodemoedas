package com.example.wlf.cotacaodemoedas.classes

import java.util.ArrayList

object Mock {
    val currenciesMock: List<String>
        get() {
            val list = ArrayList<String>()
            list.add("Selecione")
            list.add("Euro")
            list.add("Dólar")
            list.add("Libra Esterlina")
            list.add("Rublo")
            list.add("Iene")
            list.add("Dólar")
            list.add("Canadense")
            list.add("Reais")

            return list
        }

    val bitcoinMock: List<String>
        get() {
            val list = ArrayList<String>()
            list.add("Bitcoin")
            return list
        }

}
