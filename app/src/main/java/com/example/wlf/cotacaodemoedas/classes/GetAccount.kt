package com.example.wlf.cotacaodemoedas.classes

import android.content.Context
import android.widget.TextView
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG

import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley

class GetAccount {

    fun converterBitCoin(txtRes: TextView, context: Context, moeda: String, valor: String) {
        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue( context )
        val url = "https://blockchain.info/tobtc?currency=$moeda&value=$valor"

        // Request a string response from the provided URL.
        val stringRequest = StringRequest(Request.Method.GET, url,
                Response.Listener
                { response ->
                    // Display the first 500 characters of the response string.
                    txtRes.text = "$response BTC"
                },
                Response.ErrorListener { txtRes.text = "That didn't work! TEST"
                  //  Toast.makeText( getApplicationContext(), "That didn't work! TEST", Toast.LENGTH_SHORT).show() )
                }
        )
        // Add the request to the RequestQueue.
        queue.add(stringRequest)
    }
}