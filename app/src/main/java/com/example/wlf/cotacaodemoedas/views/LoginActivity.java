package com.example.wlf.cotacaodemoedas.views;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.example.wlf.cotacaodemoedas.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {
    private FirebaseAuth auth = FirebaseAuth.getInstance();

    private EditText password;
    private EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        password = findViewById(R.id.pass);
        email = findViewById(R.id.mail);

        password.setText("123456789");
        email.setText("wlf2@gmail.com");

       //   Cadastrar e autenticar Usuários

       auth.createUserWithEmailAndPassword(
                email.getText().toString(), password.getText().toString())
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if( task.isSuccessful()){
                            Log.i("CreateUser", "Usuário " + password.getText().toString() + " Cadastrado");
                        }else{
                            Log.i("CreateUser", "Erro ao cadastar Cadastrado");
                        }
                    }
                });

        // REcuperar usuário

        if (auth.getCurrentUser() != null ){
            Log.i("CreateUser", "Usuário " + email.getText().toString() + " ESTÁ LOGADO");
        }
        else{
            Log.i("CreateUser", "nÃO ESTÁ LOGADO!");
        }

    }
}
