package com.example.wlf.cotacaodemoedas.utils

import android.content.Context
import android.net.ConnectivityManager

object NetworkUtils {

    fun isConnectionAvailable(context: Context): Boolean {
        val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return manager.activeNetworkInfo != null && manager.activeNetworkInfo.isConnectedOrConnecting
    }
}