package com.example.wlf.cotacaodemoedas.views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import com.example.wlf.cotacaodemoedas.R;
import com.example.wlf.cotacaodemoedas.classes.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends Activity implements View.OnClickListener
{
    public static String URL = "";
    private Button dolarCom;
    private Button dolarTur;
    private Button euro;
    private Button bitCoin;
    private Button btnCalcBit;

    private DatabaseReference ref = FirebaseDatabase.getInstance().getReference();


    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        DatabaseReference usuarios = ref.child("usuarios");

       /*  SALVAR DADOS

       User u = new User();
        u.setNome("Wendreo");
        u.setIdade(25);
        u.setSobreNome("Fernandes");
        usuarios.child("001").setValue(u); */

       /*  RECUPERAR DADOS

       usuarios.addValueEventListener(new ValueEventListener() {
           @Override
           public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               Log.i("FIREBASE", dataSnapshot.getValue().toString());
           }

           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {
           }
       });     */




        dolarCom = findViewById( R.id.btn_dolar_comercial );
        dolarTur = findViewById( R.id.btn_dolar_turismo );
        euro = findViewById( R.id.btn_euro );
        bitCoin = findViewById( R.id.btn_bitcoin );
        btnCalcBit = findViewById( R.id.btn_calc_bit );

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); //força a tela a ficar ligada

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN); // NÃO exibe a barra superior do android

        dolarCom.setOnClickListener( this );
        dolarTur.setOnClickListener( this );
        euro.setOnClickListener( this );
        bitCoin.setOnClickListener( this );
        btnCalcBit.setOnClickListener( this );
    }

    @Override
    public void onClick( View view )
    {
        int id = view.getId();

        Intent intent = new Intent( this, DetailsActivity.class );

        if (id == R.id.btn_dolar_comercial)
        {
            URL = "http://economia.awesomeapi.com.br/USD-BRL/1";
            startActivity(intent);
        }
        else if (id == R.id.btn_dolar_turismo)
        {
            URL = "http://economia.awesomeapi.com.br/USD-BRLT/1";
            startActivity(intent);
        }
        else if (id == R.id.btn_euro)
        {
            URL = "http://economia.awesomeapi.com.br/EUR-BRL/1";
            startActivity(intent);
        }
        else if (id == R.id.btn_bitcoin)
        {
            URL = "http://economia.awesomeapi.com.br/BTC-BRL/1";
            startActivity(intent);
        }
        else if (id == R.id.btn_calc_bit)
        {
            Intent calc = new Intent(this, CalculadoraActivity.class);

            startActivity(calc);
        }
    }
}

